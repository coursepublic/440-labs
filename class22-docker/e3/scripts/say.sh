echo "Reading from file: $1"
lineNo=0
while read -r line; do
	echo "$lineNo: $line"
	lineNo=$(($lineNo+1))
done < $1
